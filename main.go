package main

import (
	"fmt"
	"sync"
)

// 定义电子设备结构体
type ElectronicDevice struct {
	ID          int
	Name        string
	Price       float64
	Description string
}

// 定义商店结构体
type Store struct {
	ID         int
	Name       string
	Location   string
	Devices    []ElectronicDevice
	Offers     []string
	Subscribed bool
	sync.Mutex
}

// 创建新商店
func NewStore(id int, name string, location string) *Store {
	return &Store{
		ID:       id,
		Name:     name,
		Location: location,
		Devices:  []ElectronicDevice{},
		Offers:   []string{},
	}
}

// 添加电子设备到商店
func (s *Store) AddDevice(device ElectronicDevice) {
	s.Lock()
	defer s.Unlock()
	s.Devices = append(s.Devices, device)
}

// 添加商店特别优惠
func (s *Store) AddOffer(offer string) {
	s.Lock()
	defer s.Unlock()
	s.Offers = append(s.Offers, offer)
}

// 定义LoyaltyProgram结构体
type LoyaltyProgram struct {
	GadgetPoints map[int]int // 顾客积分
	sync.Mutex
}

// 创建新的积分计划
func NewLoyaltyProgram() *LoyaltyProgram {
	return &LoyaltyProgram{
		GadgetPoints: make(map[int]int),
	}
}

// 增加顾客积分
func (lp *LoyaltyProgram) AddPoints(customerID int, points int) {
	lp.Lock()
	defer lp.Unlock()
	lp.GadgetPoints[customerID] += points
}

// 获取顾客积分
func (lp *LoyaltyProgram) GetPoints(customerID int) int {
	lp.Lock()
	defer lp.Unlock()
	return lp.GadgetPoints[customerID]
}
func main() {
	// 创建商店实例
	store := NewStore(1, "TechSolutions", "New York")
	// 添加设备到商店
	device1 := ElectronicDevice{
		ID:          1,
		Name:        "Smartphone",
		Price:       999.99,
		Description: "The latest smartphone with advanced features.",
	}
	store.AddDevice(device1)
	device2 := ElectronicDevice{
		ID:          2,
		Name:        "Laptop",
		Price:       1499.99,
		Description: "A powerful laptop for everyday use.",
	}
	store.AddDevice(device2)
	// 添加商店优惠
	store.AddOffer("Buy one, get one free on selected accessories.")
	// 创建积分计划实例
	lp := NewLoyaltyProgram()
	// 增加顾客积分
	lp.AddPoints(1, 10)
	lp.AddPoints(1, 20)
	// 获取顾客积分
	points := lp.GetPoints(1)
	fmt.Println("Customer 1 points:", points)
}
